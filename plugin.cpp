/* This file is part of the $PLUGIN_NAME project
   Copyright $YEAR $AUTHOR <$AUTHOR_EMAIL>

   $PLUGIN_NAME is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License (GPL) as published by the
   Free Software Foundation; version 3 of the License.

   $PLUGIN_NAME is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   $PLUGIN_NAME.  If not, see <http://www.gnu.org/licenses/>.
*/
// Self
#include <plugin.h>

// KDE
#include <kaction.h>
#include <kactioncollection.h>
#include <kapplication.h>
#include <kgenericfactory.h>
#include <klibloader.h>
#include <klocale.h>
#include <kmessagebox.h>

// KIPIPlugins
#include <libkipi/interface.h>

// Local

K_PLUGIN_FACTORY(HelloWorldFactory, registerPlugin<Plugin>();)
K_EXPORT_PLUGIN( HelloWorldFactory("kipiplugin_helloworld"))

Plugin::Plugin(QObject *parent, const QVariantList&)
: KIPI::Plugin(HelloWorldFactory::componentData(), parent, "HelloWorld")
{
}

void Plugin::setup(QWidget* widget)
{
    KIPI::Plugin::setup(widget);
    m_action = actionCollection()->addAction("helloworld");
    m_action->setText(i18n("Hello World..."));
    connect(m_action, SIGNAL(triggered()), SLOT(activate()));
    addAction(m_action);
}

void Plugin::activate()
{
    KIPI::Interface* interface = dynamic_cast<KIPI::Interface*>(parent());
    Q_ASSERT(interface);

    KIPI::ImageCollection album = interface->currentAlbum();
    QString msg = i18n("Hello KIPI World!\nCurrent album name: %1", album.name());
    KMessageBox::information(QApplication::activeWindow(), msg);
}

KIPI::Category Plugin::category(KAction* action) const
{
    if (action == m_action) {
        return KIPI::ToolsPlugin;
    }

    kWarning() << "Unrecognized action for plugin category identification";
    return KIPI::ToolsPlugin; // no warning from compiler, please
}

#include <plugin.moc>

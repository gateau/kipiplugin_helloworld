/* This file is part of the $PLUGIN_NAME project
   Copyright $YEAR $AUTHOR <$AUTHOR_EMAIL>

   $PLUGIN_NAME is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License (GPL) as published by the
   Free Software Foundation; version 3 of the License.

   $PLUGIN_NAME is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   $PLUGIN_NAME.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PLUGIN_H
#define PLUGIN_H

#include <QVariant>

#include <libkipi/plugin.h>

class KAction;

/**
 * Implementation of the KIPI::Plugin abstract class
 */
class Plugin : public KIPI::Plugin
{
    Q_OBJECT

public:
    Plugin(QObject* parent, const QVariantList& args);

    KIPI::Category category(KAction* action) const;
    void setup(QWidget* widget);

private Q_SLOTS:
    void activate();

private:
    KAction* m_action;
};

#endif // PLUGIN_H
